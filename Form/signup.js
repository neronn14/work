const username = document.getElementById("user");
const email = document.getElementById("email");
const password = document.getElementById("pwd");
const count = document.getElementById("count");
const eye = document.getElementById("eye");
const check1 = document.getElementById("check1");
const check2 = document.getElementById("check2");
const check3 = document.getElementById("check3");
const check4 = document.getElementById("check4");
const check5 = document.getElementById("check5");
const check6 = document.getElementById("check6");

var visible = false;

function show() {
    if(visible){
        password.type = "password";
        visible = false;
        eye.style.color = "#999"
    } else{
        password.type = "text";
        visible = true;
        eye.style.color = "black"
    }
} 
function emailCheck(){
    const emailVall = email.value.trim();
    if(emailVall.match(/[@.]/g)){
        check1.style.color = "green";
    } else{
        check1.style.color = "#ff314d";
    }
}

function pwdCheck(){
    const passVall = password.value.trim();
    if(passVall.length>8){
        check2.style.color = "green";
    } else{
        check2.style.color = "#ff314d";
    }

    if(passVall.length<20){
        check3.style.color = "green";
    } else{
        check3.style.color = "#ff314d";
    }

    if(passVall.match(/[0-9]/g)){
        check4.style.color = "green";
    } else{
        check4.style.color = "#ff314d";
    }

    if(passVall.match(/[!@#$%^&*_=+-]/g)){
        check5.style.color = "green";
    } else{
        check5.style.color = "#ff314d";
    }

    
    if(passVall.match(" ")){
        check6.style.color = "#ff314d";
    } else{
        check6.style.color = "green";
    }

    if(passVall.match(/[A-Z]/g)){
        check7.style.color = "green";
    } else{
        check7.style.color = "#ff314d";
    }
}

function addData(){
    // const user = document.getElementById("user").value;
    // const email = document.getElementById("email").value;
    // const pass = document.getElementById("pwd").value;

    const localUser = username.value;
    const localEmail = email.value;
    const localPass = password.value;
    
    // adding data to localstorage

    localStorage.setItem("username",localUser);
    localStorage.setItem("email",localEmail);
    localStorage.setItem("password",localPass);
}