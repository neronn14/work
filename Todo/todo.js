const form = document.querySelector("#todo-form");
const todoInput = document.querySelector("#todo");
const todoList = document.querySelector(".list-group");
const firstCardBody = document.querySelectorAll(".card-body")[0];
const secondCardBody = document.querySelectorAll(".card-body")[1];
const filter = document.querySelector("#filter");
const clearButton = document.querySelector("#clear-todos");

eventListeners();

function eventListeners(){ //! Butun event listener'lar
    form.addEventListener("submit",addTodo);
    secondCardBody.addEventListener("click",deleteTodo)  
    filter.addEventListener("keyup",filterTodos); 
    clearButton.addEventListener("click",clearAllTodos);
}

function clearAllTodos(e){
    //! Todoları interfeysdən təmizləmək

    if(confirm("Hamısını silmək istədiyinizə əminsiniz?")){
        // todoList.innerHTML = "";  //! yavaş metoddur
        
        // todoList.removeChild(todoList.firstElementChild);
        // todoList.removeChild(todoList.firstElementChild);
        // todoList.removeChild(todoList.firstElementChild);
        // todoList.removeChild(todoList.firstElementChild);

        // yada

        while(todoList.firstElementChild != null){
        todoList.removeChild(todoList.firstElementChild);

        }
    }
}

function filterTodos(e){
    // console.log(e.target.value);

    const filterValue = e.target.value.toLowerCase();
    const listItems = document.querySelectorAll(".list-group-item");

    listItems.forEach(function(listItem){
        const text = listItem.textContent.toLowerCase();
        if (text.indexOf(filterValue) === -1){
            // tapilmadi
            listItem.setAttribute("style","display : none !important");

        }
        else {
            listItem.setAttribute("style","display : block");
        }

    });
}

function deleteTodo(e){
    // console.log(e.target)
    if(e.target.className === "fa fa-remove"){
        // console.log("Silmə əməliyyatı")
        e.target.parentElement.parentElement.remove();
        showAlert("success","Todo uğurla silindi");
    }
}

function addTodo(e){
    const newTodo = todoInput.value.trim();
        // console.log(newTodo);

        if(newTodo === ""){
            /*
         <div class="alert alert-danger" role="alert">
                        This is a danger alert—check it out!
                      </div>
                      */
            showAlert("danger","Zəhmət olmasa todo daxil edin...");
        }

        else{
            addTodoToUI(newTodo);
            showAlert("success","todo uğurla əlavə edildi");
        }

        

        



        e.preventDefault();
}

function showAlert(type,message){
    const alert = document.createElement("div");
    alert.className = `alert alert-${type}`;
    alert.textContent = message;

    firstCardBody.appendChild(alert);

    //! SetTimeout

    setTimeout(function(){
        alert.remove();

    },2000)
}

function addTodoToUI(newTodo){ //! by method interfeyse todolari elave edir

    /*

    <li class="list-group-item d-flex justify-content-between">
                        Todo 1
                        <a href = "#" class ="delete-item">
                            <i class = "fa fa-remove"></i>
                        </a>

                    </li>-->
    */

    //! List Item yaratmaq
    const listItem = document.createElement("li");

    //! Link yaratmaq
    const link = document.createElement("a");
    link.href = "#";
    link.className = "delete-item";
    link.innerHTML = "<i class = 'fa fa-remove'></i>"

    listItem.className = "list-group-item d-flex justify-content-between";

    // Text Node elave etmek
    listItem.appendChild(document.createTextNode(newTodo));
    
    //! Link elave etmek
    listItem.appendChild(link);


    //! Todo List'e List Item'i elave etmek
    todoList.appendChild(listItem);
    todoInput.value = "";

    // console.log(listItem);


}